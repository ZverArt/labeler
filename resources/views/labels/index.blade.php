@extends('layout')

@section('content')
    <h1>Залитые ранее модели:</h1>
    <ul>
        @foreach($schemas as $schema)
            <li>
                <a href="{{ route('labeling-index', $schema['id']) }}">{{ $schema['name'] }}</a>
                | <a href="{{ route('schema-download', $schema['id']) }}">скачать в CSV</a>
            </li>
        @endforeach
    </ul>
@endsection
