@extends('layout')

@section('content')
    <h1>Импорт входных данных</h1>
    <form action="{{ route('labels-parse') }}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <label for="name">Имя импорта
                {!! $errors->first('name') ? '<span class="label alert">'.$errors->first('name').'</span>' : '' !!}
                <input type="text" name="name" id="name" , aria-describedby="nameHelpText" required="required">
            </label>
            <p class="help-text" id="nameHelpText">
                Это имя будет отображаться на главной и по нему можно будет отличить данные
            </p>
        </div>

        <div class="row">
            <label for="sentences">Что лейблить
                {!! $errors->first('sentences') ? '<span class="label alert">'.$errors->first('sentences').'</span>' : '' !!}
                <textarea name="sentences" id="sentences" rows="5" aria-describedby="sentencesHelpText" required="required"></textarea>
            </label>
            <p class="help-text" id="sentencesHelpText">
                Элементы для лейблинга должны располагаться по одному, на строку
            </p>
        </div>
        <hr>
        <div class="row">
            <label for="labels">Список лейблов
                {!! $errors->first('labels') ? '<span class="label alert">'.$errors->first('labels').'</span>' : '' !!}
                <textarea name="labels" id="labels" rows="5" aria-describedby="labelsHelpText" required="required"></textarea>
            </label>
            <p class="help-text" id="labelsHelpText">
                Лейблы должны быть записаны в форме labelName:labelColor, каждая пара должна располагаться на новой строке
            </p>
        </div>
        <div class="row text-center">
            <input type="submit" class="button" value="Импортировать">
        </div>
    </form>
@endsection
