<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'labels'], function () {
    Route::get('index', 'LabelsController@index')->name('labels-index');
    Route::get('import', 'LabelsController@import')->name('labels-import');
    Route::post('parse', 'LabelsController@parse')->name('labels-parse');
});

Route::get('download/{schema_id}', 'SchemaController@download')->name('schema-download');

Route::group(['prefix' => 'labeling'], function () {
    Route::get('{schema_id}', 'LabelingController@index')->name('labeling-index');
    Route::post('mark/{sentence_id}', 'LabelingController@mark')->name('labeling-mark');
    Route::post('comment/{sentence_id}', 'LabelingController@comment')->name('labeling-comment');
});
