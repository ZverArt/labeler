<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sequence extends Model
{
    public $fillable = [
        'comment',
        'schema_id',
        'sentence',
        'label'
    ];
}
