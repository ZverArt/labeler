<?php

namespace App\Http\Controllers;

use App\LabelsSchema;
use App\Sequence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LabelsController extends Controller
{
    public function index()
    {
        $schemas = LabelsSchema::all()->toArray();

        return view('labels.index', compact('schemas'));
    }

    public function import()
    {
        return view('labels.import');
    }

    public function parse(Request $request)
    {
        $this->validate($request, [
            'labels' => 'required',
            'sentences' => 'required',
            'name' => 'required',
        ]);

        $schemaName = $request->get('name');
        $labels = explode("\r\n", $request->get('labels'));
        $sentences = explode("\r\n", $request->get('sentences'));

        $schema = LabelsSchema::create([
            'name' => $schemaName,
            'labels' => $labels
        ]);

        foreach ($sentences as $sentence) {
            Sequence::create([
                'schema_id' => $schema->id,
                'sentence' => $sentence
            ]);
        }

        return redirect()->route('labels-index');
    }
}
