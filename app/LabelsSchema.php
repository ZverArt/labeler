<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabelsSchema extends Model
{
    public $casts = [
        'labels' => 'array',
    ];

    public $fillable = [
        'name',
        'labels',
    ];

    public function sentences()
    {
        return $this->hasMany(Sequence::class, 'schema_id');
    }
}
